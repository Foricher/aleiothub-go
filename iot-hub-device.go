package aleiothub

import (
	"net/http"
	"strconv"
)

// http://hassansin.github.io/request-response-pattern-using-go-channles

type DeviceService interface {
	GetDevices(offset int, limit int, format string, viewConfig bool, params map[string]interface{}) chan ResponseErr
	CreateDevice(data interface{}, notify bool) chan ResponseErr
	UpdateDevice(deviceID string, data map[string]interface{}) chan ResponseErr
	GetDevice(deviceID string, viewConfig bool) chan ResponseErr
	DeleteDevice(deviceID string, notify bool) chan ResponseErr
	GetDeviceSection(deviceID string, section string, viewConfig bool) chan ResponseErr
	UpdateDeviceSection(deviceID string, section string, data map[string]interface{}) chan ResponseErr
}

type Device struct {
	ClientWS *ClientWS
}

func NewDeviceService(ClientWS *ClientWS) DeviceService {
	deviceService := &Device{ClientWS: ClientWS}
	return deviceService
}

func (d *Device) GetDevices(offset int, limit int, format string, viewConfig bool, params map[string]interface{}) chan ResponseErr {
	_params := map[string]interface{}{
		"offset":      offset,
		"limit":       limit,
		"format":      format,
		"view-config": viewConfig,
	}
	if params != nil {
		for k, v := range params {
			_params[k] = v
		}
	}
	return (*d.ClientWS).SendRequestResponse("devices", nil, "getDevices", _params, nil)
}

func (d *Device) CreateDevice(data interface{}, notify bool) chan ResponseErr {
	params := map[string]interface{}{
		"notify": notify,
	}
	return (*d.ClientWS).SendRequestResponse("devices", nil, "createDevice", params, data)
}

func (d *Device) UpdateDevice(deviceID string, data map[string]interface{}) chan ResponseErr {
	return (*d.ClientWS).SendRequestResponse("devices", &deviceID, "updateDevice", nil, data)
}

func (d *Device) GetDevice(deviceID string, viewConfig bool) chan ResponseErr {
	params := map[string]interface{}{
		"view-config": viewConfig,
	}
	return (*d.ClientWS).SendRequestResponse("devices", &deviceID, "getDevice", params, nil)
}

func (d *Device) DeleteDevice(deviceID string, notify bool) chan ResponseErr {
	params := map[string]interface{}{
		"notify": notify,
	}
	return (*d.ClientWS).SendRequestResponse("devices", &deviceID, "deleteDevice", params, nil)
}

func (d *Device) GetDeviceSection(deviceID string, section string, viewConfig bool) chan ResponseErr {
	return (*d.ClientWS).SendRequestResponse("devices", &deviceID, "getDeviceSection", map[string]interface{}{
		"section":     section,
		"view-config": viewConfig,
	}, nil)
}

func (d *Device) UpdateDeviceSection(deviceID string, section string, data map[string]interface{}) chan ResponseErr {
	return (*d.ClientWS).SendRequestResponse("devices", &deviceID, "updateDeviceSection", map[string]interface{}{
		"section": section,
	}, data)
}

//////////////////////////////////////////////////////////////////
type DeviceHTTP struct {
	ClientHTTP *ClientHTTP
}

func NewDeviceHTTPService(ClientHTTP *ClientHTTP) DeviceService {
	deviceService := &DeviceHTTP{ClientHTTP: ClientHTTP}
	return deviceService
}

func (d *DeviceHTTP) GetDevices(offset int, limit int, format string, viewConfig bool, params map[string]interface{}) chan ResponseErr {
	urlParams := "/devices?offset=" + strconv.Itoa(offset) + "&limit=" + strconv.Itoa(limit) + "&format=" + format
	if viewConfig {
		urlParams += "&view-config=true"
	}
	if params != nil {
		for k, v := range params {
			urlParams += "&" + k + "=" + v.(string)
		}
	}
	return (*d.ClientHTTP).SendHTTPRequest(http.MethodGet, urlParams, nil)
}

func (d *DeviceHTTP) CreateDevice(data interface{}, notify bool) chan ResponseErr {
	urlParams := "/devices?notify=" + strconv.FormatBool(notify)
	return (*d.ClientHTTP).SendHTTPRequest(http.MethodPost, urlParams, data)
}

func (d *DeviceHTTP) UpdateDevice(deviceID string, data map[string]interface{}) chan ResponseErr {
	urlParams := "/devices/" + deviceID
	return (*d.ClientHTTP).SendHTTPRequest(http.MethodPut, urlParams, data)
}

func (d *DeviceHTTP) GetDevice(deviceID string, viewConfig bool) chan ResponseErr {
	urlParams := "/devices/" + deviceID
	if viewConfig {
		urlParams += "?view-config=true"
	}
	return (*d.ClientHTTP).SendHTTPRequest(http.MethodGet, urlParams, nil)
}

func (d *DeviceHTTP) DeleteDevice(deviceID string, notify bool) chan ResponseErr {
	urlParams := "/devices/" + deviceID + "?notify=" + strconv.FormatBool(notify)
	return (*d.ClientHTTP).SendHTTPRequest(http.MethodDelete, urlParams, nil)
}

func (d *DeviceHTTP) GetDeviceSection(deviceID string, section string, viewConfig bool) chan ResponseErr {
	urlParams := "/devices/" + deviceID + "/sections/" + section
	if viewConfig {
		urlParams += "?view-config=true"
	}
	return (*d.ClientHTTP).SendHTTPRequest(http.MethodGet, urlParams, nil)
}

func (d *DeviceHTTP) UpdateDeviceSection(deviceID string, section string, data map[string]interface{}) chan ResponseErr {
	urlParams := "/devices/" + deviceID + "/sections/" + section
	return (*d.ClientHTTP).SendHTTPRequest(http.MethodGet, urlParams, data)
}
