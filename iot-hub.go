package aleiothub

import (
	options "gitlab.com/Foricher/aleiothub-go/options"
	recws "gitlab.com/Foricher/aleiothub-go/recws"
	"bytes"
	"crypto/tls"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"strconv"
	"sync"
	"time"
)

type ClientWS interface {
	SetChanEvent(chanEvent chan map[string]interface{})
	SetChanConnect(chanConnect chan bool)
	IsConnected() bool
	Connect() error
	Close()
	SendMessage(data interface{}) error
	SendRequest(data requestWs) error
	SendRequestResponse(resource string, id *string, method string, params map[string]interface{}, payload interface{}) chan ResponseErr
}

type ClientHTTP interface {
	GetHttpClient() *http.Client
	SendHTTPRequest(method string, url string, data interface{}) chan ResponseErr
}

type clientWS struct {
	url         string
	options     *options.Options
	chanConnect chan bool
	chanEvent   chan map[string]interface{}
	requestID   uint64
	//	conn      *websocket.Conn
	conn             *recws.RecConn
	mutex            sync.Mutex
	pending          map[string]*call
	keepAliveTimeout time.Duration
	pingTimer        *time.Timer
	debug            bool
}

type clientHTTP struct {
	url     string
	options *options.OptionsHTTP
	debug   bool
	http    *http.Client
}

type requestWs struct {
	RequestID string                 `json:"request_id"`
	Resource  string                 `json:"resource"`
	ID        *string                `json:"id"`
	Method    string                 `json:"method"`
	Params    map[string]interface{} `json:"params"`
	Body      interface{}            `json:"body"`
}

type event struct {
	RequestID string      `json:"request_id"`
	Resource  string      `json:"resource"`
	ID        string      `json:"id"`
	Method    string      `json:"method"`
	Data      interface{} `json:"data"`
}

type response struct {
	RequestID string      `json:"request_id"`
	Resource  string      `json:"resource"`
	ID        string      `json:"id"`
	Method    string      `json:"method"`
	Data      interface{} `json:"data"`
}

type ResponseError struct {
	Status string
	Error  error
	Code   string
}

type ResponseErr struct {
	Data interface{}
	Err  *ResponseError
}

type call struct {
	Req     requestWs
	Timeout *time.Timer
	//	Res   response
	//	Res   map[string]interface{}
	Resp chan ResponseErr
	//	Error error
}

//
func NewClientWS(url string, options *options.Options, chanConnect chan bool, chanEvent chan map[string]interface{}) ClientWS {
	keepAliveTimeout := 30 * time.Second
	debug := false
	if options != nil {
		if options.KeepAliveTimeout > 0 {
			keepAliveTimeout = options.KeepAliveTimeout
		}
		debug = options.Debug
	}

	//client := &clientWS{url: url, options: options, chanEvent: make(chan interface{}), chanConnect: make(chan bool), pending: make(map[string]*call, 1), requestID: 1}
	client := &clientWS{url: url, options: options, pending: make(map[string]*call, 1), requestID: 1, keepAliveTimeout: keepAliveTimeout}
	client.chanConnect = chanConnect
	client.chanEvent = chanEvent
	client.debug = debug
	return client
}

func NewClientHTTP(urlStr string, options *options.OptionsHTTP) ClientHTTP {
	debug := false
	if options != nil {
		debug = options.Debug
	}
	client := &clientHTTP{url: urlStr, options: options}
	client.debug = debug
	client.http = &http.Client{}
	//	transport := http.DefaultTransport
	var proxy func(req *http.Request) (*url.URL, error)
	//= http.ProxyFromEnvironment
	var TLSClientConfig *tls.Config
	if options != nil {
		//			client.http.Transport.setReqHeader(options.ReqHeader)
		if len(options.HTTPProxy) > 0 {
			uProxy, _ := url.Parse(options.HTTPProxy)
			proxy = http.ProxyURL(uProxy)
		}
		if options.TLSClientConfig != nil {
			TLSClientConfig = options.TLSClientConfig
		}

	}
	client.http.Transport = &http.Transport{Proxy: proxy, TLSClientConfig: TLSClientConfig}
	return client
}

func (c *clientHTTP) GetHttpClient() *http.Client {
	return c.http
}

//func SendHTTPRequest(method string, url string, data io.Reader) chan ResponseErr {
func (c *clientHTTP) SendHTTPRequest(method string, urlParams string, data interface{}) chan ResponseErr {
	var reader io.Reader
	responseErr := make(chan ResponseErr)
	go func() {
		if data != nil {
			mdata, err := json.Marshal(data)
			if err != nil {
				responseErr <- ResponseErr{
					Err: &ResponseError{
						Error:  err,
						Status: "NOK",
					}}
				return
			} else {
				reader = bytes.NewReader(mdata)
			}
		}
		client := &http.Client{}
		reqURL := c.url + urlParams
		req, err := http.NewRequest(method, reqURL, reader)
		if err != nil {
			responseErr <- ResponseErr{
				Err: &ResponseError{
					Error:  err,
					Status: "NOK",
				}}
			return
		}
		if c.options != nil {
			if c.options.ReqHeader != nil {
				for k, v := range c.options.ReqHeader {
					for _, v1 := range v {
						req.Header.Add(k, v1)
					}
				}
			}
		}
		body, err := client.Do(req)
		if err != nil {
			responseErr <- ResponseErr{
				Err: &ResponseError{
					Error:  err,
					Status: "NOK",
				}}
			return
		}
		body1, err := ioutil.ReadAll(body.Body)
		if err != nil {
			responseErr <- ResponseErr{
				Err: &ResponseError{
					Error:  err,
					Status: "NOK",
				}}
			return
		}
		switch {
		case body.StatusCode == 404:
			responseErr <- ResponseErr{
				Err: &ResponseError{
					Error:  errors.New("Not found"),
					Status: "NOK",
				}}
			return
		case body.StatusCode < 200 || body.StatusCode >= 300:
			responseErr <- ResponseErr{
				Err: &ResponseError{
					Error:  errors.New(string(body1)),
					Status: "NOK",
				}}
			return
		}
		var _data interface{} = make(map[string]interface{})
		err2 := json.Unmarshal(body1, &_data)
		if err2 != nil {
			responseErr <- ResponseErr{
				Err: &ResponseError{
					Error:  err2,
					Status: "NOK",
				}}
			return
		}
		responseErr <- ResponseErr{Data: _data}
	}()
	return responseErr

}

func (c *clientWS) SetChanEvent(chanEvt chan map[string]interface{}) {
	c.chanEvent = chanEvt
}

func (c *clientWS) SetChanConnect(chanConnect chan bool) {
	c.chanConnect = chanConnect
}

func (c *clientWS) IsConnected() bool {
	return c.conn.IsConnected()
}

/*
func (c *clientWS) Connect1() error {
	var dialer = websocket.DefaultDialer
	if c.options != nil {
		if len(c.options.HTTPProxy) > 0 {
			uProxy, _ := url.Parse(c.options.HTTPProxy)
			dialer = &websocket.Dialer{
				Proxy: http.ProxyURL(uProxy),
			}

		}
	}

	//Set the Dialer (especially the proxy)
	//dialer := websocket.DefaultDialer ==> with this default dialer, it works !

	conn, _, err := dialer.Dial(c.url, nil) //
	//	conn, _, err := websocket.DefaultDialer.Dial(c.url, nil)
	if err != nil {
		return err
	}
	c.conn = conn
	go c.read()
	return err
}
*/

func (c *clientWS) Connect() error {
	ws := recws.RecConn{
		ConnectHandler: func(state bool) error {
			if state {
				go c.sendPing()
				go c.read()
			}
			if c.chanConnect != nil {
				select {
				case c.chanConnect <- state:
					if c.debug {
						log.Println("sent connect state:", state)
					}
					//				default:
					//					fmt.Println("no connect message sent")
				}

			}
			return nil
		},
	}
	c.conn = &ws
	/*
		var dialer = websocket.DefaultDialer
		if c.options != nil {
			if len(c.options.HTTPProxy) > 0 {
				uProxy, _ := url.Parse(c.options.HTTPProxy)
				dialer = &websocket.Dialer{
					Proxy: http.ProxyURL(uProxy),
				}

			}
		}
	*/
	//Set the Dialer (especially the proxy)
	//dialer := websocket.DefaultDialer ==> with this default dialer, it works !

	ws.DialIotHub(c.url, c.options)
	//	go c.read()
	return nil
}

func (c *clientWS) Close() {
	if c.pingTimer != nil {
		c.pingTimer.Stop()
		c.pingTimer = nil
	}
	defer c.Close()
}

func (c *clientWS) sendPing() {
	if c.IsConnected() && c.keepAliveTimeout > 0 {
		if c.pingTimer != nil {
			c.pingTimer.Stop()
		}
		c.pingTimer = time.NewTimer(c.keepAliveTimeout)
		<-c.pingTimer.C
		if c.IsConnected() {
			c.SendRequest(requestWs{
				Resource: "systems",
				Method:   "ping",
			})
			c.sendPing()
		}
	}
}

func (c *clientWS) SendMessage(data interface{}) error {
	err := c.conn.WriteJSON(data)
	return err
}

func (c *clientWS) SendRequest(data requestWs) error {
	err := c.SendMessage(data)
	return err
}

func newCall(req requestWs) *call {
	//done := make(chan bool)
	return &call{
		Req:  req,
		Resp: make(chan ResponseErr),
	}
}
func (c *clientWS) SendRequestResponse(resource string, ID *string, method string, params map[string]interface{}, payload interface{}) chan ResponseErr {
	c.mutex.Lock()
	requestID := strconv.FormatUint(c.requestID, 10)
	c.requestID++
	req := requestWs{RequestID: requestID, Resource: resource, Method: method, Params: params, Body: payload}
	if ID != nil {
		req.ID = ID
	}
	call := newCall(req)
	if c.conn == nil {
		call.Resp <- ResponseErr{
			Data: nil,
			Err: &ResponseError{
				Error:  errors.New("not connected"),
				Status: "not_connected",
			},
		}
		return call.Resp
	}
	c.pending[requestID] = call
	if c.debug {
		log.Println("SendRequestResponse=", req)
	}
	err := c.conn.WriteJSON(&req)
	if err != nil {
		delete(c.pending, requestID)
		c.mutex.Unlock()
		go func() {
			call.Resp <- ResponseErr{
				Data: nil,
				Err: &ResponseError{
					Error:  err,
					Status: "write_error",
				},
			}
		}()
		return call.Resp
		//		return nil, err
	}
	call.Timeout = time.NewTimer(30 * time.Second)
	c.mutex.Unlock()
	go func() {
		select {
		//	case <-call.Done:
		//		case <-time.After(30 * time.Second):
		case <-call.Timeout.C:
			//		call.Error = errors.New("request timeout")
			if c.debug {
				log.Printf("SendRequestResponse timeout request_id:%v\n", requestID)
			}
			c.mutex.Lock()
			delete(c.pending, requestID)
			c.mutex.Unlock()
			call.Resp <- ResponseErr{
				Data: nil,
				Err: &ResponseError{
					Error:  errors.New("request timeout"),
					Status: "request_timeout",
				},
			}
		}
	}()
	/*
		if call.Error != nil {
			return nil, call.Error
		}
		return call.Res["body"], nil
	*/
	return call.Resp
}

func (c *clientWS) read() {
	var err error
	for err == nil {
		var event map[string]interface{}
		err = c.conn.ReadJSON(&event)
		if err != nil {
			err = fmt.Errorf("error reading message:%q", err)
			continue
		}
		if c.debug {
			log.Println("RCV message=", event)
		}
		// fmt.Printf("received message: %+v\n", res)
		if c.chanEvent != nil {
			go func() {
				c.chanEvent <- event
				if c.debug {
					log.Println("read sent message", event)
				}
			}()
			/*
				select {
				case c.chanEvent <- &event:
					fmt.Println("read sent message", event)
				default:
					fmt.Println("read no event message sent")
				}
			*/
		}
		message := event["message"].(string)
		requestID := event["request_id"]
		if requestID != nil {
			if c.debug {
				log.Println("read request_id mess=", requestID, message)
			}
			c.mutex.Lock()
			call := c.pending[requestID.(string)]
			delete(c.pending, requestID.(string))
			c.mutex.Unlock()
			if call == nil {
				if c.debug {
					log.Println("read no pending")
				}
				err = errors.New("no pending request found")
				continue
			}
			if call.Timeout != nil {
				call.Timeout.Stop()
			}
			switch message {
			case "response_success":
				if c.debug {
					log.Println("read response_success")
				}
				call.Resp <- ResponseErr{
					Data: event["body"],
					Err:  nil,
				}
			case "response_error":
				var _error string
				var _status string
				err := event["error"]
				if err.(map[string]interface{})["error"] != nil {
					_error = fmt.Sprintf("%v", err.(map[string]interface{})["error"])
					//					_error = err.(map[string]interface{})["error"].(string)
				}
				if err.(map[string]interface{})["status"] != nil {
					_status = fmt.Sprintf("%v", err.(map[string]interface{})["status"])
					//					_status = err.(map[string]interface{})["status"].(string)
				}
				log.Println("read response_error")
				call.Resp <- ResponseErr{
					Data: nil,
					Err: &ResponseError{
						Error:  errors.New(_error),
						Status: _status,
					},
				}
			default:

			}
			//			call.Res = event
			//			call.Done <- true
		}
	}
	/*
		c.mutex.Lock()
		for _, call := range c.pending {
			call.Error = err
			call.Done <- true
		}
		c.mutex.Unlock()
	*/
}

/*
var addr = flag.String("addr", "localhost:8080", "http service address")

func main22() {
	flag.Parse()
	log.SetFlags(0)

	interrupt := make(chan os.Signal, 1)
	signal.Notify(interrupt, os.Interrupt)

	u := url.URL{Scheme: "ws", Host: *addr, Path: "/echo"}
	log.Printf("connecting to %s", u.String())

	c, _, err := websocket.DefaultDialer.Dial(u.String(), nil)
	if err != nil {
		log.Fatal("dial:", err)
	}
	defer c.Close()

	done := make(chan struct{})

	go func() {
		defer close(done)
		for {
			_, message, err := c.ReadMessage()
			if err != nil {
				log.Println("read:", err)
				return
			}
			log.Printf("recv: %s", message)
		}
	}()

	ticker := time.NewTicker(time.Second)
	defer ticker.Stop()

	for {
		select {
		case <-done:
			return
		case t := <-ticker.C:
			err := c.WriteMessage(websocket.TextMessage, []byte(t.String()))
			if err != nil {
				log.Println("write:", err)
				return
			}
		case <-interrupt:
			log.Println("interrupt")

			// Cleanly close the connection by sending a close message and then
			// waiting (with timeout) for the server to close the connection.
			err := c.WriteMessage(websocket.CloseMessage, websocket.FormatCloseMessage(websocket.CloseNormalClosure, ""))
			if err != nil {
				log.Println("write close:", err)
				return
			}
			select {
			case <-done:
			case <-time.After(time.Second):
			}
			return
		}
	}
}
*/
