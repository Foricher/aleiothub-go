package recws

import (
	"crypto/tls"
	"log"
	"net/http"
	"net/url"
	"time"

	"github.com/gorilla/websocket"
)

type Options struct {
	HTTPProxy        string
	ReqHeader        http.Header
	TLSClientConfig  *tls.Config
	KeepAliveTimeout time.Duration
	Debug            bool
}

func (rc *RecConn) setDialer(handshakeTimeout time.Duration) {
	rc.mu.Lock()
	defer rc.mu.Unlock()

	rc.dialer = &websocket.Dialer{
		HandshakeTimeout: handshakeTimeout,
	}
}

func (rc *RecConn) DialIotHub(urlStr string, options *Options) {
	urlStr, err := rc.parseURL(urlStr)

	if err != nil {
		log.Fatalf("Dial: %v", err)
	}

	// Config
	rc.setURL(urlStr)
	rc.setDefaultRecIntvlMin()
	rc.setDefaultRecIntvlMax()
	rc.setDefaultRecIntvlFactor()
	rc.setDefaultHandshakeTimeout()
	rc.setDialer(rc.getHandshakeTimeout())
	//	rc.KeepAliveTimeout = 30 * time.Second
	//TODO Ping
	rc.dialer = websocket.DefaultDialer
	rc.dialer.HandshakeTimeout = rc.HandshakeTimeout
	rc.dialer.Proxy = nil
	if options != nil {
		rc.setReqHeader(options.ReqHeader)
		if len(options.HTTPProxy) > 0 {
			uProxy, _ := url.Parse(options.HTTPProxy)
			rc.dialer.Proxy = http.ProxyURL(uProxy)
		}
		if options.TLSClientConfig != nil {
			rc.dialer.TLSClientConfig = options.TLSClientConfig
		}
	}
	// Connect
	go rc.connect()

	// wait on first attempt
	time.Sleep(rc.getHandshakeTimeout())
}
