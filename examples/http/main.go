package main

import (
	aleiothub "gitlab.com/Foricher/aleiothub-go"
	options gitlab.com/Foricher/aleiothub-go"/options"
	"encoding/base64"
	"log"
	"net/http"
)

func basicAuthHTTP(username, password string) string {
	auth := username + ":" + password
	return base64.StdEncoding.EncodeToString([]byte(auth))
}

func main() {
	//	client := aleiothub.NewClientHTTP("http://localhost:9000/api", nil)
	header := http.Header{
		"Authorization": {"Basic " + basicAuthHTTP("tdd", "tdd")},
	}

	client := aleiothub.NewClientHTTP("https://iothub-dimension-data.ale-custo.com/api", &options.OptionsHTTP{
		HTTPProxy: "http://192.168.254.49:8080",
		ReqHeader: header,
	})

	deviceService := aleiothub.NewDeviceHTTPService(&client)
	resp := <-deviceService.GetDevices(0, 3, "id", false, nil)
	if resp.Err != nil {
		log.Printf("GetDevices err %v", resp.Err)
	} else {
		arr := resp.Data.([]interface{})
		log.Printf("GetDevices len=%v", len(arr))
		log.Printf("GetDevices res=%v", arr)
	}

	resp1 := <-deviceService.GetDevice("sga$100$shades$shade", true)
	if resp1.Err != nil {
		log.Printf("GetDevice err %v", resp1.Err)
	} else {
		log.Printf("GetDevice res=%v", resp1.Data)
	}

	resp1 = <-deviceService.GetDevice("iot$tdd$tag$EE061FC8062D", true)
	if resp1.Err != nil {
		log.Printf("GetDevice err %v", resp1.Err)
	} else {
		log.Printf("GetDevice res=%v", resp1.Data)
	}
}
