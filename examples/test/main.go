package main

import (
	aleiothub "gitlab.com/Foricher/aleiothub-go"
	"encoding/base64"
	"log"
	"time"
)

func basicAuth(username, password string) string {
	auth := username + ":" + password
	return base64.StdEncoding.EncodeToString([]byte(auth))
}

func main() {
	chanEvent := make(chan map[string]interface{})
	chanConnect := make(chan bool)
	client := aleiothub.NewClientWS("ws://localhost:9000/api/ws", nil, chanConnect, chanEvent)
	/*
		header := http.Header{
			"Authorization": {"Basic " + basicAuth("tdd", "tdd")},
		}

		client := aleiothub.NewClientWS("wss://iothub-dimension-data.ale-custo.com/api/ws", &recws.Options{
			HTTPProxy: "http://192.168.254.49:8080",
			ReqHeader: header,
		}, chanConnect, chanEvent)
	*/
	deviceService := aleiothub.NewDeviceService(&client)
	client.Connect()
	ticker := time.NewTicker(20 * time.Second)

	process := func() {
		var resp = <-deviceService.GetDevices(0, 2, "id", nil)
		if resp.Err != nil {
			log.Printf("GetDevices err %v", resp.Err)
		} else {
			arr := resp.Data.([]interface{})
			log.Printf("GetDevices len=%v", len(arr))
			log.Printf("GetDevices res=%v", arr)
		}
		/*
			resp = <-deviceService.GetDevice("sga$100$shades$shade")
			if resp.Err != nil {
				log.Printf("GetDevice err %v", resp.Err)
			} else {
				log.Printf("GetDevice res=%v", resp.Data)
			}
		*/
		/*
			var resp = <-deviceService.DeleteDevice("device_id_test", false)
			if resp.Err != nil {
				log.Printf("DeleteDevice err %v", resp.Err)
			} else {
				log.Printf("DeleteDevice res=%v", resp.Data)
			}

			data := map[string]interface{}{
				"id":   "device_id_test",
				"type": "iot.light.simple",
				"name": "test",
				"sections": map[string]interface{}{
					"config": map[string]interface{}{
						"p1": "1234",
					},
				},
			}

			resp = <-deviceService.CreateDevice(data, false)
			if resp.Err != nil {
				log.Printf("CreateDevice err %v", resp.Err)
			} else {
				log.Printf("CreateDevice res=%v", resp.Data)
			}

			resp = <-deviceService.GetDevice("device_id_test", false)
			if resp.Err != nil {
				log.Printf("GetDevice err %v", resp.Err)
			} else {
				log.Printf("GetDevice res=%v", resp.Data)
			}

			resp = <-deviceService.GetDevice("bad_device_id_test", false)
			if resp.Err != nil {
				log.Printf("GetDevice err %v", resp.Err)
			} else {
				log.Printf("GetDevice res=%v", resp.Data)
			}

			resp = <-deviceService.DeleteDevice("device_id_test", false)
			if resp.Err != nil {
				log.Printf("DeleteDevice err %v", resp.Err)
			} else {
				log.Printf("DeleteDevice res=%v", resp.Data)
			}
		*/
	}

	log.Printf("STARTED")
	for {
		select {
		case evt := <-chanEvent:
			log.Println("EVT=", evt)
		case connected := <-chanConnect:
			log.Println("Connected=", connected)
			if connected {
				go process()
			}
		case <-ticker.C:
			if client.IsConnected() {
				//				resp := <-deviceService.GetDevice("sga$100$shades$shade", true)
				resp := <-deviceService.GetDevices(0, 100, "id", nil)
				if resp.Err != nil {
					log.Printf("Timer GetDevice err %v", resp.Err)
				} else {
					log.Printf("Timer GetDevice res=%v", resp.Data)
				}
			} else {
				log.Printf("Not Connected")
			}
		}
	}
}
