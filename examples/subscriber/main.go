package main

import (
	aleiothub "gitlab.com/Foricher/aleiothub-go"
	"encoding/base64"
	"log"
)

func basicAuth(username, password string) string {
	auth := username + ":" + password
	return base64.StdEncoding.EncodeToString([]byte(auth))
}

func main() {
	chanEvent := make(chan map[string]interface{})
	chanConnect := make(chan bool)
	client := aleiothub.NewClientWS("ws://localhost:9000/api/ws", nil, chanConnect, chanEvent)
	/*
		header := http.Header{
			"Authorization": {"Basic " + basicAuth("tdd", "tdd")},
		}

		client := aleiothub.NewClientWS("wss://iothub-dimension-data.ale-custo.com/api/ws", &recws.Options{
			HTTPProxy: "http://192.168.254.49:8080",
			ReqHeader: header,
		}, chanConnect, chanEvent)
	*/
	deviceService := aleiothub.NewDeviceService(&client)
	subcriberService := aleiothub.NewSubscriberService(&client)
	client.Connect()
	process := func() {

		subscription := map[string]interface{}{
			"name": "subscriber go test",
			"device_topics": []map[string]interface{}{{
				"id": "sga$100$shades$shade",
				"filters": []map[string]interface{}{
					{"section": "state"},
					{"section": "actions"},
				},
			},
			},
		}
		log.Println("subscription=", subscription)
		var resp = <-subcriberService.CreateSubscriber(subscription, nil)
		if resp.Err != nil {
			log.Printf("CreateSubscriber err %v", resp.Err)
		} else {
			log.Printf("CreateSubscriber res=%v", resp.Data)
		}

		resp = <-deviceService.GetDevices(0, 2, "id", nil)
		if resp.Err != nil {
			log.Printf("GetDevices err %v", resp.Err)
		} else {
			arr := resp.Data.([]interface{})
			log.Printf("GetDevices len=%v", len(arr))
			log.Printf("GetDevices res=%v", arr)
		}
	}

	log.Printf("STARTED Subscriber")
	for {
		select {
		case evt := <-chanEvent:
			if evt["message"] == "event" {
				log.Println("EVT=", evt)
				switch evt["resource"] {
				case "devices":
					switch evt["event"] {
					case "updated_section":
						log.Println("section=", evt["section"])
					}
				}
			}
		case connected := <-chanConnect:
			log.Println("Connected=", connected)
			if connected {
				go process()
			}
		}
	}
}
