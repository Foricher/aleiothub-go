package aleiothub

import (
	"net/http"
	"strconv"
)

// http://hassansin.github.io/request-response-pattern-using-go-channles

type SchemaDeviceService interface {
	GetSchemas(offset int, limit int, format string) chan ResponseErr
	CreateSchema(data interface{}) chan ResponseErr
	UpdateSchema(schemaID string, data map[string]interface{}) chan ResponseErr
	GetSchema(schemaID string) chan ResponseErr
	DeleteSchema(schemaID string) chan ResponseErr
}

type SchemaDevice struct {
	ClientWS *ClientWS
}

func NewSchemaDeviceService(ClientWS *ClientWS) SchemaDeviceService {
	schemaService := &SchemaDevice{ClientWS: ClientWS}
	return schemaService
}

func (s *SchemaDevice) GetSchemas(offset int, limit int, format string) chan ResponseErr {
	_params := map[string]interface{}{
		"offset": offset,
		"limit":  limit,
		"format": format,
	}
	return (*s.ClientWS).SendRequestResponse("device-schemas", nil, "getDevices", _params, nil)
}

func (s *SchemaDevice) CreateSchema(data interface{}) chan ResponseErr {
	return (*s.ClientWS).SendRequestResponse("device-schemas", nil, "createSchema", nil, data)
}

func (s *SchemaDevice) UpdateSchema(schemaID string, data map[string]interface{}) chan ResponseErr {
	return (*s.ClientWS).SendRequestResponse("device-schemas", &schemaID, "updateSchema", nil, data)
}

func (s *SchemaDevice) GetSchema(schemaID string) chan ResponseErr {
	return (*s.ClientWS).SendRequestResponse("device-schemas", &schemaID, "getSchema", nil, nil)
}

func (s *SchemaDevice) DeleteSchema(schemaID string) chan ResponseErr {
	return (*s.ClientWS).SendRequestResponse("device-schemas", &schemaID, "deleteSchema", nil, nil)
}

//////////////////////////////////////////////////////////////////
type SchemaDeviceHTTP struct {
	ClientHTTP *ClientHTTP
}

func NewSchemaDeviceHTTPService(ClientHTTP *ClientHTTP) SchemaDeviceService {
	schemaDeviceService := &SchemaDeviceHTTP{ClientHTTP: ClientHTTP}
	return schemaDeviceService
}

func (d *SchemaDeviceHTTP) GetSchemas(offset int, limit int, format string) chan ResponseErr {
	urlParams := "/schemas/devices?offset=" + strconv.Itoa(offset) + "&limit=" + strconv.Itoa(limit) + "&format=" + format
	return (*d.ClientHTTP).SendHTTPRequest(http.MethodGet, urlParams, nil)
}

func (d *SchemaDeviceHTTP) CreateSchema(data interface{}) chan ResponseErr {
	urlParams := "/schemas/devices"
	return (*d.ClientHTTP).SendHTTPRequest(http.MethodPost, urlParams, data)
}

func (d *SchemaDeviceHTTP) UpdateSchema(schemaID string, data map[string]interface{}) chan ResponseErr {
	urlParams := "/schemas/devices/" + schemaID
	return (*d.ClientHTTP).SendHTTPRequest(http.MethodPut, urlParams, data)
}

func (d *SchemaDeviceHTTP) GetSchema(schemaID string) chan ResponseErr {
	urlParams := "/schemas/devices/" + schemaID
	return (*d.ClientHTTP).SendHTTPRequest(http.MethodGet, urlParams, nil)
}

func (d *SchemaDeviceHTTP) DeleteSchema(schemaID string) chan ResponseErr {
	urlParams := "/schemas/devices/" + schemaID
	return (*d.ClientHTTP).SendHTTPRequest(http.MethodDelete, urlParams, nil)
}
