package aleiothub

import (
	"net/http"
	"regexp"
	"strconv"
)

// http://hassansin.github.io/request-response-pattern-using-go-channles

type GatewayService interface {
	GetGateways(offset int, limit int, format string, params map[string]interface{}) chan ResponseErr
	CreateGateway(data interface{}) chan ResponseErr
	UpdateGateway(gatewayID string, data map[string]interface{}) chan ResponseErr
	GetGateway(gatewayID string) chan ResponseErr
	DeleteGateway(gatewayID string) chan ResponseErr
	UpdatePartialGateway(gatewayID string, path string, data map[string]interface{}) chan ResponseErr
	SendGatewayControllerStateToHub(gatewayID string, controllerURL string, stateData map[string]interface{}) chan ResponseErr
}

type Gateway struct {
	ClientWS *ClientWS
}

func NewGatewayService(ClientWS *ClientWS) GatewayService {
	gatewayService := &Gateway{ClientWS: ClientWS}
	return gatewayService
}

func (g *Gateway) GetGateways(offset int, limit int, format string, params map[string]interface{}) chan ResponseErr {
	_params := map[string]interface{}{
		"offset": offset,
		"limit":  limit,
		"format": format,
	}
	if params != nil {
		for k, v := range params {
			_params[k] = v
		}
	}
	return (*g.ClientWS).SendRequestResponse("gateways", nil, "getGateways", _params, nil)
}

func (g *Gateway) CreateGateway(data interface{}) chan ResponseErr {
	return (*g.ClientWS).SendRequestResponse("gateways", nil, "createGateway", nil, data)
}

func (g *Gateway) UpdateGateway(gatewayID string, data map[string]interface{}) chan ResponseErr {
	return (*g.ClientWS).SendRequestResponse("gateways", &gatewayID, "updateGateway", nil, data)
}

func (g *Gateway) GetGateway(gatewayID string) chan ResponseErr {
	return (*g.ClientWS).SendRequestResponse("gateways", &gatewayID, "getGateway", nil, nil)
}

func (g *Gateway) DeleteGateway(gatewayID string) chan ResponseErr {
	return (*g.ClientWS).SendRequestResponse("gateways", &gatewayID, "deleteGateway", nil, nil)
}

func (g *Gateway) UpdatePartialGateway(gatewayID string, path string, data map[string]interface{}) chan ResponseErr {
	return (*g.ClientWS).SendRequestResponse("gateways", &gatewayID, "updatePartialGateway", map[string]interface{}{
		"path": path,
	}, data)
}

func (g *Gateway) SendGatewayControllerStateToHub(gatewayID string, controllerURL string, stateData map[string]interface{}) chan ResponseErr {
	r, _ := regexp.Compile("\\.")
	str := r.ReplaceAllString(controllerURL, "_")
	return (*g).UpdatePartialGateway(gatewayID, "state.controllers."+str, stateData)
}

//////////////////////////////////////////////////////////////////
type GatewayHTTP struct {
	ClientHTTP *ClientHTTP
}

func NewGatewayHTTPService(ClientHTTP *ClientHTTP) GatewayService {
	gatewayService := &GatewayHTTP{ClientHTTP: ClientHTTP}
	return gatewayService
}

func (g *GatewayHTTP) GetGateways(offset int, limit int, format string, params map[string]interface{}) chan ResponseErr {
	urlParams := "/gateways?offset=" + strconv.Itoa(offset) + "&limit=" + strconv.Itoa(limit) + "&format=" + format
	if params != nil {
		for k, v := range params {
			urlParams += "&" + k + "=" + v.(string)
		}
	}
	return (*g.ClientHTTP).SendHTTPRequest(http.MethodGet, urlParams, nil)
}

func (g *GatewayHTTP) CreateGateway(data interface{}) chan ResponseErr {
	urlParams := "/gateways"
	return (*g.ClientHTTP).SendHTTPRequest(http.MethodPost, urlParams, data)
}

func (g *GatewayHTTP) UpdateGateway(gatewayID string, data map[string]interface{}) chan ResponseErr {
	urlParams := "/gateways/" + gatewayID
	return (*g.ClientHTTP).SendHTTPRequest(http.MethodPut, urlParams, data)
}

func (g *GatewayHTTP) GetGateway(gatewayID string) chan ResponseErr {
	urlParams := "/gateways/" + gatewayID
	return (*g.ClientHTTP).SendHTTPRequest(http.MethodGet, urlParams, nil)
}

func (g *GatewayHTTP) DeleteGateway(gatewayID string) chan ResponseErr {
	urlParams := "/gateways/" + gatewayID
	return (*g.ClientHTTP).SendHTTPRequest(http.MethodDelete, urlParams, nil)
}

func (g *GatewayHTTP) UpdatePartialGateway(gatewayID string, path string, data map[string]interface{}) chan ResponseErr {
	urlParams := "/gateways/" + gatewayID + "/update/" + path
	return (*g.ClientHTTP).SendHTTPRequest(http.MethodPut, urlParams, data)
}

func (g *GatewayHTTP) SendGatewayControllerStateToHub(gatewayID string, controllerURL string, stateData map[string]interface{}) chan ResponseErr {
	r, _ := regexp.Compile("\\.")
	str := r.ReplaceAllString(controllerURL, "_")
	return (*g).UpdatePartialGateway(gatewayID, "state.controllers."+str, stateData)
}
