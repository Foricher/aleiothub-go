package aleiothub

import (
	"net/http"
	"strconv"
)

// http://hassansin.github.io/request-response-pattern-using-go-channles

type SubscriberService interface {
	GetSubscribers(offset int, limit int, format string) chan ResponseErr
	CreateSubscriber(subscription interface{}, subscriberID string) chan ResponseErr
	UpdateSubscriber(subscriberID string, data map[string]interface{}) chan ResponseErr
	GetSubscriber(schemaID string) chan ResponseErr
	DeleteSubscriber(schemaID string) chan ResponseErr
}

type Subscriber struct {
	ClientWS *ClientWS
}

func NewSubscriberService(ClientWS *ClientWS) SubscriberService {
	subscriberService := &Subscriber{ClientWS: ClientWS}
	return subscriberService
}

func (s *Subscriber) GetSubscribers(offset int, limit int, format string) chan ResponseErr {
	_params := map[string]interface{}{
		"offset": offset,
		"limit":  limit,
		"format": format,
	}
	return (*s.ClientWS).SendRequestResponse("subscribers", nil, "getSubscribers", _params, nil)
}

func (s *Subscriber) CreateSubscriber(subscription interface{}, subscriberID string) chan ResponseErr {
	var pSubscriberID *string
	if len(subscriberID) > 0 {
		pSubscriberID = &subscriberID
	}
	return (*s.ClientWS).SendRequestResponse("subscribers", pSubscriberID, "createSubscriber", nil, subscription)
}

func (s *Subscriber) UpdateSubscriber(subscriberID string, data map[string]interface{}) chan ResponseErr {
	return (*s.ClientWS).SendRequestResponse("subscribers", &subscriberID, "updateSubscriber", nil, data)
}

func (s *Subscriber) GetSubscriber(subscriberID string) chan ResponseErr {
	return (*s.ClientWS).SendRequestResponse("subscribers", &subscriberID, "getSubscriber", nil, nil)
}

func (s *Subscriber) DeleteSubscriber(subscriberID string) chan ResponseErr {
	return (*s.ClientWS).SendRequestResponse("subscribers", &subscriberID, "deleteSubscriber", nil, nil)
}

//////////////////////////////////////////////////////////////////
type SubscriberHTTP struct {
	ClientHTTP *ClientHTTP
}

func NewSubscriberHTTPService(ClientHTTP *ClientHTTP) SubscriberService {
	subscriberService := &SubscriberHTTP{ClientHTTP: ClientHTTP}
	return subscriberService
}

func (s *SubscriberHTTP) GetSubscribers(offset int, limit int, format string) chan ResponseErr {
	urlParams := "/subscribers?offset=" + strconv.Itoa(offset) + "&limit=" + strconv.Itoa(limit) + "&format=" + format
	return (*s.ClientHTTP).SendHTTPRequest(http.MethodGet, urlParams, nil)
}

func (s *SubscriberHTTP) CreateSubscriber(subscription interface{}, subscriberID string) chan ResponseErr {
	urlParams := "/subscribers"
	if len(subscriberID) > 0 {
		urlParams += "/" + subscriberID
	}
	return (*s.ClientHTTP).SendHTTPRequest(http.MethodPost, urlParams, subscription)
}

func (g *SubscriberHTTP) UpdateSubscriber(subscriberID string, data map[string]interface{}) chan ResponseErr {
	urlParams := "/subscribers/" + subscriberID
	return (*g.ClientHTTP).SendHTTPRequest(http.MethodPut, urlParams, data)
}

func (g *SubscriberHTTP) GetSubscriber(subscriberID string) chan ResponseErr {
	urlParams := "/subscribers/" + subscriberID
	return (*g.ClientHTTP).SendHTTPRequest(http.MethodGet, urlParams, nil)
}

func (g *SubscriberHTTP) DeleteSubscriber(subscriberID string) chan ResponseErr {
	urlParams := "/subscribers/" + subscriberID
	return (*g.ClientHTTP).SendHTTPRequest(http.MethodDelete, urlParams, nil)
}
