package options

import (
	"crypto/tls"
	"net/http"
	"time"
)

type Options struct {
	HTTPProxy        string
	ReqHeader        http.Header
	TLSClientConfig  *tls.Config
	KeepAliveTimeout time.Duration
	Debug            bool
}

type OptionsHTTP struct {
	HTTPProxy        string
	ReqHeader        http.Header
	TLSClientConfig  *tls.Config
	KeepAliveTimeout time.Duration
	Debug            bool
}
